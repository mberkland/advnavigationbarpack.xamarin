using System;
using System.Drawing;

using MonoTouch.Foundation;
using MonoTouch.UIKit;
using System.Threading.Tasks;
using System.Threading;

namespace ADVNavigationBarPack.Xamarin
{
	public partial class ADVNavigationBarPack_XamarinViewController : UIViewController
	{
		public ADVNavigationBarPack_XamarinViewController () : base ("ADVNavigationBarPack_XamarinViewController", null)
		{
		}


		public override void DidReceiveMemoryWarning ()
		{
			// Releases the view if it doesn't have a superview.
			base.DidReceiveMemoryWarning ();
			
			// Release any cached data, images, etc that aren't in use.
		}
		
		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();

			this.Title = "Style";

			// Perform any additional setup after loading the view, typically from a nib.
			var adapter = new EnumTableAdapter<ThemeChoice>((choice)=>{

				var first = new PickBackgroundViewController(choice){ Title = "Background"};
				this.NavigationController.PushViewController(first,true);
			
			}, 
			withAccessoryFinder: (choice)=>{
				return UITableViewCellAccessory.DisclosureIndicator;
			});

			this.tblView.Source = adapter;
			this.tblView.BackgroundColor = UIColor.Clear;



		}

		public override void ViewWillAppear (bool animated)
		{
			AppearanceManager.Instance.ApplyBackground(this.View);
		}
		
		public override bool ShouldAutorotateToInterfaceOrientation (UIInterfaceOrientation toInterfaceOrientation)
		{
			// Return true for supported orientations
			return (toInterfaceOrientation != UIInterfaceOrientation.PortraitUpsideDown);
		}
	}

	public class EnumTableAdapter<T> : UITableViewSource where T : struct
	{
		string[] data;
		private Action<T> callback;
		private Func<T,UIImage> imageFinder;
		private Func<T, UITableViewCellAccessory> accessoryFinder;

		public EnumTableAdapter(Action<T> withCallback, 
		                        Func<T,UIImage> withImageCustomizer=null,
		                        Func<T,UITableViewCellAccessory> withAccessoryFinder = null)
		{
			this.callback = withCallback;
			this.imageFinder = withImageCustomizer;
			//if we don't have one populate with none
			if(withAccessoryFinder == null)
				withAccessoryFinder = (choice)=>{
					return UITableViewCellAccessory.None;
			};

			this.accessoryFinder = withAccessoryFinder;
			this.data = Enum.GetNames(typeof(T));
		}

		public override int RowsInSection(UITableView view, int section)
		{
			return this.data.Length;
		}

		public override UITableViewCell GetCell (UITableView tableView, NSIndexPath indexPath)
		{
			UITableViewCell cell = tableView.DequeueReusableCell("cellID");
			if(cell==null)
				cell = new UITableViewCell(UITableViewCellStyle.Default,"cellID");

			var item = this.data[indexPath.Row];
			UIImage img=null;

			if(imageFinder!=null)
				img = imageFinder(resolveType(item));

			if(img!=null)
			{
				cell.ImageView.Image = img;
			}

			cell.Accessory = accessoryFinder(resolveType(item));

			cell.TextLabel.Text = this.data[indexPath.Row];
			return cell;
		}

		public override void RowSelected (UITableView tableView, NSIndexPath indexPath)
		{
			//they got one
			var item = this.data[indexPath.Row];
			callback(resolveType(item));
		}

		private T resolveType(string v)
		{
			T inst;
			Enum.TryParse<T>(v,out inst);
			return inst;
		}



	}


}

