
using System;
using System.Drawing;

using MonoTouch.Foundation;
using MonoTouch.UIKit;
using System.Threading;

namespace ADVNavigationBarPack.Xamarin
{
	public partial class ShowStyleViewController : UINavigationController
	{
		public ShowStyleViewController (ThemeChoice choice, BackgroundType bg) : base ("ShowStyleViewController", null)
		{
			AppearanceManager.Instance.SetTheme(choice, bg);

		}
		
		public override void DidReceiveMemoryWarning ()
		{
			// Releases the view if it doesn't have a superview.
			base.DidReceiveMemoryWarning ();
			
			// Release any cached data, images, etc that aren't in use.
		}
		
		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();

			this.Title = "Navigation Controller";

			// Perform any additional setup after loading the view, typically from a nib.

			//create a regular done button for the first view
			var rButton = new UIBarButtonItem(UIBarButtonSystemItem.Done, (send,args)=>{
				//close ourselves
				this.DismissViewController(true,null);
			});

			//create our 2 views
			var firstView = new UIViewController(){Title = "First Page"};
			var secondView = new UIViewController() { Title = "Second"};

			//if the template requires a textless back button
			if(AppearanceManager.Instance.CanBackButtonHaveText())
			{
				firstView.NavigationItem.BackBarButtonItem = new UIBarButtonItem{ Title = "Back"};
				firstView.NavigationItem.RightBarButtonItem = rButton;
			}
			else
			{
				firstView.NavigationItem.BackBarButtonItem = new UIBarButtonItem(){Title="    "};
				firstView.NavigationItem.RightBarButtonItem = rButton;
			}

			//set the background for the first view and second view
			AppearanceManager.Instance.ApplyBackground(firstView.View);
			AppearanceManager.Instance.ApplyBackground(secondView.View);

			//apply any custom navbar stuff (translucent)
			AppearanceManager.Instance.ApplyNavbar(this.NavigationBar);

			//make a button to go to the second view if they want
			var btnGoSecond = new UIButton (new Rectangle (20,50, 280, 50));
			btnGoSecond.SetTitle ("Go back to second page", UIControlState.Normal);
			btnGoSecond.ApplyButtonStyle ((ButtonStyleTypes.Blue), true);
			btnGoSecond.AddTarget ((s, e) =>  {
				this.PushViewController(secondView,true);
			},UIControlEvent.TouchUpInside);
			//add it to the first view page
			firstView.View.Add(btnGoSecond);

			//show our first view
			this.PushViewController(firstView,true);


			//start a thread that in a second, will transition to our second page
			Thread t = new Thread(()=>{ Thread.Sleep(1000);
				this.InvokeOnMainThread(()=>{
					//show the second view
					this.PushViewController(secondView,true);

					//create a sample button for each style...
					MakeButtons (secondView);

					//get a right bar button
                    var btn = AppearanceManager.Instance.GetRightBarButtonTemplate(withHandler: (sender, args) =>
                    {
                        this.DismissViewController(true, null);
                    });

					//set it as the right bar
					secondView.NavigationItem.RightBarButtonItem = btn;
					
				});
			});
			t.Start();

		}

		void MakeButtons (UIViewController second)
		{
			int y = 30;
			var scrollView = new AutoSizeScrollView();
			scrollView.ExtraHeight = 40;
			second.View.Add(scrollView);
			scrollView.Frame = second.GetMaxUseableFrame();


			foreach (var style in Enum.GetValues (typeof(ButtonStyleTypes))) {
				var tempButton = new UIButton (new Rectangle (20, y, 280, 50));
				tempButton.SetTitle (style.ToString()+" button - Close View", UIControlState.Normal);
				tempButton.ApplyButtonStyle ((ButtonStyleTypes)style);
				tempButton.AddTarget ((s, e) =>  {
					this.DismissViewController (true, null);
				}, UIControlEvent.TouchUpInside);
				//second.View.Add (tempButton);
				scrollView.Add(tempButton);
				y += 55;//move our position down
			}
		}
	}
}

