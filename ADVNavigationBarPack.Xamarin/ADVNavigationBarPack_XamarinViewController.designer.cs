// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the Xcode designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using MonoTouch.Foundation;

namespace ADVNavigationBarPack.Xamarin
{
	[Register ("ADVNavigationBarPack_XamarinViewController")]
	partial class ADVNavigationBarPack_XamarinViewController
	{
		[Outlet]
		MonoTouch.UIKit.UITableView tblView { get; set; }
		
		void ReleaseDesignerOutlets ()
		{
			if (tblView != null) {
				tblView.Dispose ();
				tblView = null;
			}
		}
	}
}
