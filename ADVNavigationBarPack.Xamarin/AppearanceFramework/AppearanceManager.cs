using System;
using MonoTouch.UIKit;
using MonoTouch.CoreGraphics;
using MonoTouch.CoreData;
using System.Drawing;

namespace AppearanceFramework
{
    public class AppearanceManager
    {
        static AppearanceManager()
        {
            Instance = new AppearanceManager();
        }
        private AppearanceManager() { }

        public static AppearanceManager Instance { get; set; }


        public ThemeChoice CurrentTheme { get; set; }
		public BackgroundType CurrentBackground {get;set;}
        public AppearanceDef CurrentAppearance { get; set; }

		//this returns a basic setup for some fonts and colors that 
		//a lot of the templates use
        private AppearanceDef GetBaseAppearance()
        {
            //return a base represenation of this thing...
            return new AppearanceDef
            {
                Buttons = new ButtonDef
                {
                    capInsets = new float[] { 0.0f, 9.0f, 0.0f, 5.0f },
                    titleColor = UIColor.White,
                    titleFont = UIFont.BoldSystemFontOfSize(10.0f)

                },
                Navigation = new NavigationBarDef
                {
                    titleFont = UIFont.BoldSystemFontOfSize(23.0f),
                    titleShadowColor = UIColor.Black,
                    titleShadowOffset = new UIOffset(0.0f, 1.0f),
                    titleColor = UIColor.White

                },
                View = new ViewDef
                {

                }
            };
        }

        private void PopulateImagesByFolderStyleName(string withFolder, string withStyle)
        {
			//looks up from the bundle the images for back buttons, navbar bg, and a generic view background image
            CurrentAppearance.Buttons.backButtonImage = UIImage.FromBundle(string.Format("TemplateImages/{0}/Assets/{1}/{0}-{2}-back-button", withFolder, withStyle, withStyle.ToLower().Replace(" ", "-")));
            CurrentAppearance.Navigation.backGroundImage = UIImage.FromBundle(string.Format("TemplateImages/{0}/Assets/{1}/{0}-{2}-menu-bar", withFolder, withStyle, withStyle.ToLower().Replace(" ", "-")));
            CurrentAppearance.View.backGroundImage = UIImage.FromBundle(string.Format("TemplateImages/{0}/Assets/{1}/{0}-{2}-background.jpg", withFolder, withStyle, withStyle.ToLower().Replace(" ", "-")));
        }

        private void PopulateImagesWithNamedFiles(string withFolder, string withBarImage, string withButtonImage, string withViewBackground)
        {
            CurrentAppearance.Buttons.backButtonImage = UIImage.FromBundle(string.Format("TemplateImages/{0}/Assets/{1}", withFolder, withButtonImage));
            CurrentAppearance.Navigation.backGroundImage = UIImage.FromBundle(string.Format("TemplateImages/{0}/Assets/{1}", withFolder, withBarImage));
            CurrentAppearance.View.backGroundImage = UIImage.FromBundle(string.Format("TemplateImages/{0}/Assets/{1}", withFolder, withViewBackground));
        }

        public void SetTheme(ThemeChoice withTheme, BackgroundType withBackground = 
		                     BackgroundType.Default, Action<AppearanceDef> withCustomizer= null)
        {
            CurrentTheme = withTheme;
			CurrentBackground = withBackground;
            CurrentAppearance = GetBaseAppearance();

            //big ass switch block...
            switch (CurrentTheme)
            {
                case ThemeChoice.Piccadilly1:
                    #region Piccadilly 1
                    PopulateImagesByFolderStyleName("1", "Dark");
                    #endregion
                    break;
                case ThemeChoice.Piccadilly2:
                    #region Piccadilly 2
                    PopulateImagesByFolderStyleName("1", "Grey");
                    #endregion
                    break;
                case ThemeChoice.Piccadilly3:
                    #region Piccadilly 3
                    PopulateImagesByFolderStyleName("1", "Light");
                    #endregion
                    break;
                case ThemeChoice.VauxhallBlue:
                    #region Vauxhall Blue
                    PopulateImagesByFolderStyleName("2", "Blue");
                    CurrentAppearance.Buttons.titleFont = UIFont.BoldSystemFontOfSize(12.0f);
                    CurrentAppearance.Navigation.titleFont = UIFont.FromName("ChalkboardSE-Bold", 19.0f);
                    #endregion
                    break;
                case ThemeChoice.VauxhallGreen:
                    #region Vauxhall Green
                    PopulateImagesByFolderStyleName("2", "Green");
                    CurrentAppearance.Buttons.titleFont = UIFont.BoldSystemFontOfSize(12.0f);
                    CurrentAppearance.Navigation.titleFont = UIFont.FromName("ChalkboardSE-Bold", 19.0f);
                    #endregion
                    break;
                case ThemeChoice.VauxhallOrange:
                    #region Vauxhall Orange
                    PopulateImagesByFolderStyleName("2", "Orange");
                    CurrentAppearance.Buttons.titleFont = UIFont.BoldSystemFontOfSize(12.0f);
                    CurrentAppearance.Navigation.titleFont = UIFont.FromName("ChalkboardSE-Bold", 19.0f);
                    #endregion
                    break;
                case ThemeChoice.VauxhallRed:
                    #region Vauxhall Red
                    PopulateImagesByFolderStyleName("2", "Red");
                    CurrentAppearance.Buttons.titleFont = UIFont.BoldSystemFontOfSize(12.0f);
                    CurrentAppearance.Navigation.titleFont = UIFont.FromName("ChalkboardSE-Bold", 19.0f);
                    #endregion
                    break;
                case ThemeChoice.PimlicoBirch:
                    #region Pimlico Birch
                    PopulateImagesByFolderStyleName("3", "Birch Wood");
                    CurrentAppearance.Buttons.titleFont = UIFont.BoldSystemFontOfSize(12.0f);
                    #endregion
                    break;
                case ThemeChoice.PimlicoCherry:
                    #region Pimlico Cherry
                    PopulateImagesByFolderStyleName("3", "Cherry Wood");
                    CurrentAppearance.Buttons.titleFont = UIFont.BoldSystemFontOfSize(12.0f);
                    #endregion
                    break;
                case ThemeChoice.PimlicoOak:
                    #region Pimlico Oak
                    PopulateImagesByFolderStyleName("3", "Oak Wood");
                    CurrentAppearance.Buttons.titleFont = UIFont.BoldSystemFontOfSize(12.0f);
                    #endregion
                    break;
                case ThemeChoice.HendonDark:
                    #region Hendon Dark
                    PopulateImagesByFolderStyleName("4", "Dark");
                    CurrentAppearance.Buttons.capInsets = new float[] { 0, 0, 0, 15.0f };
                    CurrentAppearance.Navigation.adjustmentAmount = -3.0f;
                    CurrentAppearance.Navigation.titleFont = UIFont.FromName("EuphemiaUCAS-Bold", 23.0f);
                    #endregion
                    break;
                case ThemeChoice.HendonGrey:
                    #region Hendon Grey
                    PopulateImagesByFolderStyleName("4", "Grey");
                    CurrentAppearance.Buttons.capInsets = new float[] { 0, 0, 0, 15.0f };
                    CurrentAppearance.Navigation.adjustmentAmount = -3.0f;
                    CurrentAppearance.Navigation.titleFont = UIFont.FromName("EuphemiaUCAS-Bold", 23.0f);
                    #endregion
                    break;
                case ThemeChoice.HendonLight:
                    #region Hendon Light
                    PopulateImagesByFolderStyleName("4", "Light");
                    CurrentAppearance.Buttons.capInsets = new float[] { 0, 0, 0, 15.0f };
                    CurrentAppearance.Navigation.adjustmentAmount = -3.0f;
                    CurrentAppearance.Navigation.titleFont = UIFont.FromName("EuphemiaUCAS-Bold", 23.0f);
                    #endregion
                    break;
                case ThemeChoice.CamdenBlue:
                    #region Camden Blue
                    PopulateImagesByFolderStyleName("5", "Blue");
                    CurrentAppearance.Buttons.capInsets = new float[] { 0, 0, 0, 20.0f };
                    CurrentAppearance.Navigation.adjustmentAmount = -3.0f;
                    CurrentAppearance.Navigation.titleFont = UIFont.FromName("MarkerFelt-Wide", 23.0f);
                    #endregion
                    break;
                case ThemeChoice.CamdenGreen:
                    #region Camden Green
                    PopulateImagesByFolderStyleName("5", "Green");
                    CurrentAppearance.Buttons.capInsets = new float[] { 0, 0, 0, 20.0f };
                    CurrentAppearance.Navigation.titleFont = UIFont.FromName("MarkerFelt-Wide", 23.0f);
                    #endregion
                    break;
                case ThemeChoice.CamdenMagenta:
                    #region Camden Magenta
                    PopulateImagesByFolderStyleName("5", "Magenta");
                    CurrentAppearance.Buttons.capInsets = new float[] { 0, 0, 0, 20.0f };
                    CurrentAppearance.Navigation.titleFont = UIFont.FromName("MarkerFelt-Wide", 23.0f);
                    #endregion
                    break;
                case ThemeChoice.CamdenOrange:
                    #region Camden Orange
                    PopulateImagesByFolderStyleName("5", "Orange");
                    CurrentAppearance.Buttons.capInsets = new float[] { 0, 0, 0, 20.0f };
                    CurrentAppearance.Navigation.titleFont = UIFont.FromName("MarkerFelt-Wide", 23.0f);
                    #endregion
                    break;
                case ThemeChoice.CamdenRed:
                    #region Camdne Red
                    PopulateImagesByFolderStyleName("5", "Red");
                    CurrentAppearance.Buttons.capInsets = new float[] { 0, 0, 0, 20.0f };
                    CurrentAppearance.Navigation.titleFont = UIFont.FromName("MarkerFelt-Wide", 23.0f);
                    #endregion
                    break;
                case ThemeChoice.HolbornDark:
                    #region Holborn Dark
                    PopulateImagesByFolderStyleName("6", "Black");
                    CurrentAppearance.Buttons.capInsets = new float[] { 0, 5.0f, 0, 5.0f };
                    CurrentAppearance.Navigation.adjustmentAmount = -1.0f;
                    CurrentAppearance.Navigation.backgroundTranslucent = true;
                    CurrentAppearance.Navigation.titleFont = UIFont.FromName("BradleyHandITCTT-Bold", 23.0f);
                    #endregion
                    break;
                case ThemeChoice.HolbornGreen:
                    #region Holborn Green
                    PopulateImagesByFolderStyleName("6", "Green");
                    CurrentAppearance.Buttons.capInsets = new float[] { 0, 5.0f, 0, 5.0f };
                    CurrentAppearance.Navigation.adjustmentAmount = -1.0f;
                    CurrentAppearance.Navigation.backgroundTranslucent = true;
                    CurrentAppearance.Navigation.titleFont = UIFont.FromName("BradleyHandITCTT-Bold", 23.0f);
                    #endregion
                    break;
                case ThemeChoice.KensingtonForrest:
                    #region Kensington Forrest
                    PopulateImagesByFolderStyleName("7", "Forrest");
                    CurrentAppearance.Buttons.capInsets = new float[] { 0, 9.0f, 0, 5.0f };
                    CurrentAppearance.Buttons.titleFont = UIFont.FromName("Arial", 13.0f);
                    CurrentAppearance.Navigation.titleFont = UIFont.FromName("BradleyHandITCTT-Bold", 23.0f);
                    #endregion
                    break;
                case ThemeChoice.KensingtonNavy:
                    #region Kensington Navy
                    PopulateImagesByFolderStyleName("7", "Navy");
                    CurrentAppearance.Buttons.capInsets = new float[] { 0, 9.0f, 0, 5.0f };
                    CurrentAppearance.Buttons.titleFont = UIFont.FromName("Arial", 13.0f);
                    CurrentAppearance.Navigation.titleFont = UIFont.FromName("BradleyHandITCTT-Bold", 23.0f);
                    #endregion
                    break;
                case ThemeChoice.KensingtonPurple:
                    #region Kensington Purple
                    PopulateImagesByFolderStyleName("7", "Purple");
                    CurrentAppearance.Buttons.capInsets = new float[] { 0, 9.0f, 0, 5.0f };
                    CurrentAppearance.Buttons.titleFont = UIFont.FromName("Arial", 13.0f);
                    CurrentAppearance.Navigation.titleFont = UIFont.FromName("BradleyHandITCTT-Bold", 23.0f);
                    #endregion
                    break;
                case ThemeChoice.BelsizeBlue:
                    #region Belsize Blue
                    PopulateImagesByFolderStyleName("8", "Blue");
                    CurrentAppearance.Buttons.capInsets = new float[] { 0, 0, 0, 0 };
                    CurrentAppearance.Navigation.titleFont = UIFont.FromName("TrebuchetMS", 23.0f);
                    #endregion
                    break;
                case ThemeChoice.BelsizeGreen:
                    #region Belsize Green
                    PopulateImagesByFolderStyleName("8", "Green");
                    CurrentAppearance.Buttons.capInsets = new float[] { 0, 0, 0, 0 };
                    CurrentAppearance.Navigation.titleFont = UIFont.FromName("TrebuchetMS", 23.0f);
                    #endregion
                    break;
                case ThemeChoice.BelsizeRed:
                    #region Belsize Red
                    PopulateImagesByFolderStyleName("8", "Red");
                    CurrentAppearance.Buttons.capInsets = new float[] { 0, 0, 0, 0 };
                    CurrentAppearance.Navigation.titleFont = UIFont.FromName("TrebuchetMS", 23.0f);
                    #endregion
                    break;
                case ThemeChoice.Richmond1:
                    #region Richmond 1
                    PopulateImagesWithNamedFiles(
                        withFolder: "9",
                        withBarImage: "9-menu-bar-a",
                        withButtonImage: "9-back-button-a",
                        withViewBackground: "9-background-a.jpg"
                        );
                    CurrentAppearance.Buttons.capInsets = new float[] { 0, 9, 0, 5 };
                    CurrentAppearance.Navigation.titleFont = UIFont.FromName("Avenir-Black", 23.0f);

                    #endregion

                    break;

                case ThemeChoice.Richmond2:
                    #region Richmond 2
                    PopulateImagesWithNamedFiles(
                        withFolder: "9",
                        withBarImage: "9-menu-bar-a",
                        withButtonImage: "9-back-button-b",
                        withViewBackground: "9-background-a.jpg"
                        );
                    CurrentAppearance.Buttons.capInsets = new float[] { 0, 9, 0, 5 };
                    CurrentAppearance.Navigation.titleFont = UIFont.FromName("Avenir-Black", 23.0f);

                    #endregion

                    break;

                case ThemeChoice.Richmond3:
                    #region Richmond 3
                    PopulateImagesWithNamedFiles(
                        withFolder: "9",
                        withBarImage: "9-menu-bar-a",
                        withButtonImage: "9-back-button-c",
                        withViewBackground: "9-background-a.jpg"
                        );
                    CurrentAppearance.Buttons.capInsets = new float[] { 0, 9, 0, 5 };
                    CurrentAppearance.Navigation.titleFont = UIFont.FromName("Avenir-Black", 23.0f);

                    #endregion

                    break;

                case ThemeChoice.Richmond4:
                    #region Richmond 4
                    PopulateImagesWithNamedFiles(
                        withFolder: "9",
                        withBarImage: "9-menu-bar-a",
                        withButtonImage: "9-back-button-d",
                        withViewBackground: "9-background-a.jpg"
                        );
                    CurrentAppearance.Buttons.capInsets = new float[] { 0, 9, 0, 5 };
                    CurrentAppearance.Navigation.titleFont = UIFont.FromName("Avenir-Black", 23.0f);

                    #endregion

                    break;

                case ThemeChoice.Richmond5:
                    #region Richmond 5
                    PopulateImagesWithNamedFiles(
                        withFolder: "9",
                        withBarImage: "9-menu-bar-b",
                        withButtonImage: "9-back-button-a",
                        withViewBackground: "9-background-b.jpg"
                        );
                    CurrentAppearance.Buttons.capInsets = new float[] { 0, 9, 0, 5 };
                    CurrentAppearance.Navigation.titleFont = UIFont.FromName("Avenir-Black", 23.0f);

                    #endregion

                    break;

                case ThemeChoice.Richmond6:
                    #region Richmond 6
                    PopulateImagesWithNamedFiles(
                        withFolder: "9",
                        withBarImage: "9-menu-bar-b",
                        withButtonImage: "9-back-button-b",
                        withViewBackground: "9-background-b.jpg"
                        );
                    CurrentAppearance.Buttons.capInsets = new float[] { 0, 9, 0, 5 };
                    CurrentAppearance.Navigation.titleFont = UIFont.FromName("Avenir-Black", 23.0f);

                    #endregion

                    break;

                case ThemeChoice.Richmond7:
                    #region Richmond 7
                    PopulateImagesWithNamedFiles(
                        withFolder: "9",
                        withBarImage: "9-menu-bar-b",
                        withButtonImage: "9-back-button-c",
                        withViewBackground: "9-background-b.jpg"
                        );
                    CurrentAppearance.Buttons.capInsets = new float[] { 0, 9, 0, 5 };
                    CurrentAppearance.Navigation.titleFont = UIFont.FromName("Avenir-Black", 23.0f);

                    #endregion

                    break;


                case ThemeChoice.Richmond8:
                    #region Richmond 8
                    PopulateImagesWithNamedFiles(
                        withFolder: "9",
                        withBarImage: "9-menu-bar-b",
                        withButtonImage: "9-back-button-d",
                        withViewBackground: "9-background-b.jpg"
                        );
                    CurrentAppearance.Buttons.capInsets = new float[] { 0, 9, 0, 5 };
                    CurrentAppearance.Navigation.titleFont = UIFont.FromName("Avenir-Black", 23.0f);

                    #endregion

                    break;

                case ThemeChoice.SloaneDark:
                    #region Sloane Dark
                    PopulateImagesByFolderStyleName("10", "Dark");
                    CurrentAppearance.Buttons.capInsets = new float[] { 0, 30, 0, 0 };
                    CurrentAppearance.Navigation.adjustmentAmount = 1.0f;
                    CurrentAppearance.Navigation.titleFont = UIFont.FromName("TrebuchetMS", 23.0f);

                    #endregion

                    break;

                case ThemeChoice.SloaneLight:
                    #region Sloane Light
                    PopulateImagesByFolderStyleName("10", "Light");
                    CurrentAppearance.Buttons.capInsets = new float[] { 0, 30, 0, 0 };
                    CurrentAppearance.Navigation.adjustmentAmount = 1.0f;
                    CurrentAppearance.Navigation.titleFont = UIFont.FromName("TrebuchetMS", 21.0f);
					CurrentAppearance.Navigation.titleColor = UIColor.Black;

                    #endregion

                    break;

                case ThemeChoice.SloaneMedium:
                    #region Sloan Medium
                    PopulateImagesByFolderStyleName("10", "Medium");
                    CurrentAppearance.Buttons.capInsets = new float[] { 0, 30, 0, 0 };
                    CurrentAppearance.Navigation.adjustmentAmount = 1.0f;
                    CurrentAppearance.Navigation.titleFont = UIFont.FromName("TrebuchetMS", 23.0f);
					CurrentAppearance.Navigation.titleColor = UIColor.Black;

                    #endregion

                    break;





            }

			if(withCustomizer!=null)
				withCustomizer(CurrentAppearance);

            //apply it
            ApplyTheme(CurrentAppearance);
        }

        private UIImage LoadImageFromBundle(string withFolder=null, string withStyle=null, string withName=null, string withAbsolutePath=null)
        {
            if (!string.IsNullOrEmpty(withAbsolutePath))
                return UIImage.FromBundle(withAbsolutePath);
                     

            if(string.IsNullOrEmpty(withStyle))
                return UIImage.FromBundle(string.Format("TemplateImages/{0}/Assets/{1}", withFolder, withName));
            else
                return UIImage.FromBundle(string.Format("TemplateImages/{0}/Assets/{1}/{2}", withFolder, withStyle, withName));
        }
		
		public bool CanBackButtonHaveText()
		{
			switch(CurrentTheme)
			 {
                case ThemeChoice.HendonDark:
				case ThemeChoice.HendonGrey:
                case ThemeChoice.HendonLight:
				case ThemeChoice.CamdenBlue:
				case ThemeChoice.CamdenGreen:
				case ThemeChoice.CamdenMagenta:
                case ThemeChoice.CamdenOrange:
                case ThemeChoice.CamdenRed:
				case ThemeChoice.BelsizeBlue:
				case ThemeChoice.BelsizeGreen:
				case ThemeChoice.BelsizeRed:
				case ThemeChoice.SloaneDark:
                case ThemeChoice.SloaneLight:
                case ThemeChoice.SloaneMedium:
					return false;
				default:
                    break;
            }

			//everyone else can have back button text
			return true;
		}

        public UIBarButtonItem GetRightBarButtonTemplate(
			string withPath = null,
			EventHandler withHandler = null, 
			string withTitle=null,
			Action<UIButton> withCustomizer = null)
        {


            //get the settings button by default for the template that is active
            var result = new UIButton(UIButtonType.Custom);
            UIImage bgImage = null;
            UIImage primaryImage = null;

            if (withPath != null)
                primaryImage = UIImage.FromBundle(withPath);

            if (withHandler != null)
                result.AddTarget(withHandler, UIControlEvent.TouchUpInside);
            

            switch (CurrentTheme)
            {
				case ThemeChoice.None:
			    case ThemeChoice.Piccadilly1:
                    if (primaryImage == null)
                        primaryImage = LoadImageFromBundle(withFolder: "1", withStyle: "Dark", withName: "1-dark-settings-button");
                    bgImage = LoadImageFromBundle(withFolder: "1", withStyle: "Dark", withName: "1-dark-bar-button");
                    break;
                case ThemeChoice.Piccadilly2:
                    if (primaryImage == null)
                        primaryImage = LoadImageFromBundle(withFolder: "1", withStyle: "Grey", withName: "1-grey-settings-button");
                    bgImage = LoadImageFromBundle(withFolder: "1", withStyle: "Grey", withName: "1-grey-bar-button");
                    break;
                case ThemeChoice.Piccadilly3:
                    if (primaryImage == null)
                        primaryImage = LoadImageFromBundle(withFolder: "1", withStyle: "Light", withName: "1-light-settings-button");
                    bgImage = LoadImageFromBundle(withFolder: "1", withStyle: "Light", withName: "1-light-bar-button");
                    break;
                case ThemeChoice.VauxhallBlue:
                    if (primaryImage == null)
                        primaryImage = LoadImageFromBundle(withFolder: "2", withStyle: "Blue", withName: "2-blue-settings-button");
                    bgImage = LoadImageFromBundle(withFolder: "2", withStyle: "Blue", withName: "2-blue-bar-button");
                    break;
                case ThemeChoice.VauxhallGreen:
                    if (primaryImage == null)
                        primaryImage = LoadImageFromBundle(withFolder: "2", withStyle: "Green", withName: "2-green-settings-button");
                    bgImage = LoadImageFromBundle(withFolder: "2", withStyle: "Green", withName: "2-green-bar-button");
                    break;
                case ThemeChoice.VauxhallOrange:
                    if (primaryImage == null)
                        primaryImage = LoadImageFromBundle(withFolder: "2", withStyle: "Orange", withName: "2-orange-settings-button");
                    bgImage = LoadImageFromBundle(withFolder: "2", withStyle: "Orange", withName: "2-orange-bar-button");
                    break;
                case ThemeChoice.VauxhallRed:
                    if (primaryImage == null)
                        primaryImage = LoadImageFromBundle(withFolder: "2", withStyle: "Red", withName: "2-red-settings-button");
                    bgImage = LoadImageFromBundle(withFolder: "2", withStyle: "Red", withName: "2-red-bar-button");
                    break;
                case ThemeChoice.PimlicoBirch:
                    //totally custom for the wood
                    bgImage = LoadImageFromBundle(withFolder: "3", withStyle: "Birch Wood", withName: "3-birch-wood-button");
                    result.TitleLabel.Font = UIFont.FromName("Helvetica", 12.0f);
                    result.TitleLabel.ShadowColor = UIColor.Black;
                    result.TitleLabel.ShadowOffset = new SizeF(0.0f, -1.0f);
                    result.SetTitle(withTitle ?? "Store", UIControlState.Normal);
                    result.SetTitleColor(UIColor.White, UIControlState.Normal);
                    break;
                case ThemeChoice.PimlicoCherry:
                     bgImage = LoadImageFromBundle(withFolder: "3", withStyle: "Cherry Wood", withName: "3-cherry-wood-button");
                    result.TitleLabel.Font = UIFont.FromName("Helvetica", 12.0f);
                    result.TitleLabel.ShadowColor = UIColor.Black;
                    result.TitleLabel.ShadowOffset = new SizeF(0.0f, -1.0f);
                    result.SetTitle(withTitle ?? "Store", UIControlState.Normal);
                    result.SetTitleColor(UIColor.White, UIControlState.Normal);
                    break;
                case ThemeChoice.PimlicoOak:
                     bgImage = LoadImageFromBundle(withFolder: "3", withStyle: "Oak Wood", withName: "3-oak-wood-button");
                    result.TitleLabel.Font = UIFont.FromName("Helvetica", 12.0f);
                    result.TitleLabel.ShadowColor = UIColor.Black;
                    result.TitleLabel.ShadowOffset = new SizeF(0.0f, -1.0f);
                    result.SetTitle(withTitle ?? "Store", UIControlState.Normal);
                    result.SetTitleColor(UIColor.White, UIControlState.Normal);
                    break;
                case ThemeChoice.HendonDark:
					if (primaryImage == null)
						primaryImage = LoadImageFromBundle(withFolder: "4", withStyle: "Dark", withName: "4-dark-settings-button");
					bgImage = LoadImageFromBundle(withFolder: "4", withStyle: "Dark", withName: "4-dark-bar-button");
				    break;
                case ThemeChoice.HendonGrey:
                    if (primaryImage == null)
						primaryImage = LoadImageFromBundle(withFolder: "4", withStyle: "Grey", withName: "4-grey-settings-button");
					bgImage = LoadImageFromBundle(withFolder: "4", withStyle: "Grey", withName: "4-grey-bar-button");
				    break;
                case ThemeChoice.HendonLight:
					if (primaryImage == null)
						primaryImage = LoadImageFromBundle(withFolder: "4", withStyle: "Light", withName: "4-light-settings-button");
					bgImage = LoadImageFromBundle(withFolder: "4", withStyle: "Light", withName: "4-light-bar-button");
				    break;
                case ThemeChoice.CamdenBlue:
					if (primaryImage == null)
						primaryImage = LoadImageFromBundle(withFolder: "5", withStyle: "Blue", withName: "5-blue-settings-button");
					bgImage = LoadImageFromBundle(withFolder: "5", withStyle: "Blue", withName: "5-blue-bar-button");
				    break;
                case ThemeChoice.CamdenGreen:
					if (primaryImage == null)
						primaryImage = LoadImageFromBundle(withFolder: "5", withStyle: "Green", withName: "5-green-settings-button");
					bgImage = LoadImageFromBundle(withFolder: "5", withStyle: "Green", withName: "5-green-bar-button");
				    break;
                case ThemeChoice.CamdenMagenta:
                    if (primaryImage == null)
						primaryImage = LoadImageFromBundle(withFolder: "5", withStyle: "Magenta", withName: "5-magenta-settings-button");
					bgImage = LoadImageFromBundle(withFolder: "5", withStyle: "Magenta", withName: "5-magenta-bar-button");
                    break;
                case ThemeChoice.CamdenOrange:
                    if (primaryImage == null)
						primaryImage = LoadImageFromBundle(withFolder: "5", withStyle: "Orange", withName: "5-orange-settings-button");
					bgImage = LoadImageFromBundle(withFolder: "5", withStyle: "Orange", withName: "5-orange-bar-button");
                    break;
                case ThemeChoice.CamdenRed:
                    if (primaryImage == null)
						primaryImage = LoadImageFromBundle(withFolder: "5", withStyle: "Red", withName: "5-red-settings-button");
					bgImage = LoadImageFromBundle(withFolder: "5", withStyle: "Red", withName: "5-red-bar-button");
                    break;
                case ThemeChoice.HolbornDark:
                    if (primaryImage == null)
						primaryImage = LoadImageFromBundle(withFolder: "6", withStyle: "Black", withName: "6-black-settings-button");
					bgImage = LoadImageFromBundle(withFolder: "6", withStyle: "Black", withName: "6-black-bar-button");
                    break;
                case ThemeChoice.HolbornGreen:
                    if (primaryImage == null)
						primaryImage = LoadImageFromBundle(withFolder: "6", withStyle: "Green", withName: "6-green-settings-button");
					bgImage = LoadImageFromBundle(withFolder: "6", withStyle: "Green", withName: "6-green-bar-button");
                    break;
                case ThemeChoice.KensingtonForrest:
				if (primaryImage == null)
						primaryImage = LoadImageFromBundle(withFolder: "7", withStyle: "Forrest", withName: "7-forrest-settings-button");
					bgImage = LoadImageFromBundle(withFolder: "7", withStyle: "Forrest", withName: "7-forrest-bar-button");
                    break;
                case ThemeChoice.KensingtonNavy:
					if (primaryImage == null)
						primaryImage = LoadImageFromBundle(withFolder: "7", withStyle: "Navy", withName: "7-navy-settings-button");
					bgImage = LoadImageFromBundle(withFolder: "7", withStyle: "Navy", withName: "7-navy-bar-button");
                    break;
                case ThemeChoice.KensingtonPurple:
					if (primaryImage == null)
						primaryImage = LoadImageFromBundle(withFolder: "7", withStyle: "Purple", withName: "7-purple-settings-button");
					bgImage = LoadImageFromBundle(withFolder: "7", withStyle: "Purple", withName: "7-purple-bar-button");
                    break;
                case ThemeChoice.BelsizeBlue:
					if (primaryImage == null)
						primaryImage = LoadImageFromBundle(withFolder: "8", withStyle: "Blue", withName: "8-blue-settings-button");
					bgImage = LoadImageFromBundle(withFolder: "8", withStyle: "Blue", withName: "8-blue-bar-button");
                    break;
                case ThemeChoice.BelsizeGreen:
					if (primaryImage == null)
						primaryImage = LoadImageFromBundle(withFolder: "8", withStyle: "Green", withName: "8-green-settings-button");
					bgImage = LoadImageFromBundle(withFolder: "8", withStyle: "Green", withName: "8-green-bar-button");
                    break;
                case ThemeChoice.BelsizeRed:
					if (primaryImage == null)
						primaryImage = LoadImageFromBundle(withFolder: "8", withStyle: "Red", withName: "8-red-settings-button");
					bgImage = LoadImageFromBundle(withFolder: "8", withStyle: "Red", withName: "8-red-bar-button");
                    break;
                case ThemeChoice.Richmond1:
                    if (primaryImage == null)
						primaryImage = LoadImageFromBundle(withFolder: "9", withName: "9-settings-button");
					bgImage = LoadImageFromBundle(withFolder: "9", withName: "9-blank-button-a");
                    break;
                case ThemeChoice.Richmond2:
                    if (primaryImage == null)
						primaryImage = LoadImageFromBundle(withFolder: "9", withName: "9-settings-button");
					bgImage = LoadImageFromBundle(withFolder: "9", withName: "9-blank-button-b");
                    break;
                case ThemeChoice.Richmond3:
                    if (primaryImage == null)
						primaryImage = LoadImageFromBundle(withFolder: "9", withName: "9-settings-button");
					bgImage = LoadImageFromBundle(withFolder: "9", withName: "9-blank-button-c");
                    break;
                case ThemeChoice.Richmond4:
                    if (primaryImage == null)
						primaryImage = LoadImageFromBundle(withFolder: "9", withName: "9-settings-button");
					bgImage = LoadImageFromBundle(withFolder: "9", withName: "9-blank-button-d");
                    break;
                case ThemeChoice.Richmond5:
                    if (primaryImage == null)
						primaryImage = LoadImageFromBundle(withFolder: "9", withName: "9-settings-button");
					bgImage = LoadImageFromBundle(withFolder: "9", withName: "9-blank-button-a");
                    break;
                case ThemeChoice.Richmond6:
                    if (primaryImage == null)
						primaryImage = LoadImageFromBundle(withFolder: "9", withName: "9-settings-button");
					bgImage = LoadImageFromBundle(withFolder: "9", withName: "9-blank-button-b");
                    break;
                case ThemeChoice.Richmond7:
                    if (primaryImage == null)
						primaryImage = LoadImageFromBundle(withFolder: "9", withName: "9-settings-button");
					bgImage = LoadImageFromBundle(withFolder: "9", withName: "9-blank-button-c");
                    break;
                case ThemeChoice.Richmond8:
                    if (primaryImage == null)
						primaryImage = LoadImageFromBundle(withFolder: "9", withName: "9-settings-button");
					bgImage = LoadImageFromBundle(withFolder: "9", withName: "9-blank-button-d");
                    break;
                case ThemeChoice.SloaneDark:
                    if (primaryImage == null)
						primaryImage = LoadImageFromBundle(withFolder: "10", withStyle: "Dark", withName: "10-dark-settings-button");
					bgImage = LoadImageFromBundle(withFolder: "10", withStyle: "Dark", withName: "10-dark-bar-button");
                    break;
                case ThemeChoice.SloaneLight:
                    if (primaryImage == null)
						primaryImage = LoadImageFromBundle(withFolder: "10", withStyle: "Light", withName: "10-light-settings-button");
					bgImage = LoadImageFromBundle(withFolder: "10", withStyle: "Light", withName: "10-light-bar-button");
                    break;
                case ThemeChoice.SloaneMedium:
                    if (primaryImage == null)
						primaryImage = LoadImageFromBundle(withFolder: "10", withStyle: "Medium", withName: "10-medium-settings-button");
					bgImage = LoadImageFromBundle(withFolder: "10", withStyle: "Medium", withName: "10-medium-bar-button");
                    break;
                default:
                    break;
            }

            //this happens to every button...
			if(primaryImage!=null)
			{
            	result.Frame = new RectangleF(0.0f, 0.0f, primaryImage.Size.Width, primaryImage.Size.Height);
            	result.SetImage(primaryImage, UIControlState.Normal);
			}
			else
			{
				//need some sort of frame size
				result.Frame = new RectangleF(0.0f,0.0f,bgImage.Size.Width, bgImage.Size.Height);
			}
            result.SetBackgroundImage(bgImage, UIControlState.Normal);

			if(withCustomizer!=null)
				withCustomizer(result);
			      

            return new UIBarButtonItem(result);

        }

        public void ApplyTheme(AppearanceDef def, Action<AppearanceDef> withCustomizer=null)
        {
			if(CurrentTheme == ThemeChoice.None)
				return;
            //this actually applies all the theme settings...
            if (def.Navigation != null)
            {
                UINavigationBar.Appearance.SetBackgroundImage(def.Navigation.backGroundImage, UIBarMetrics.Default);
                var back = def.Buttons.backButtonImage.CreateResizableImage(new UIEdgeInsets(def.Buttons.capInsets[0], def.Buttons.capInsets[1], def.Buttons.capInsets[2], def.Buttons.capInsets[3]));
                UIBarButtonItem.Appearance.SetBackButtonBackgroundImage(back, UIControlState.Normal, UIBarMetrics.Default);

                var attr = new UITextAttributes
                {
                    Font = def.Navigation.titleFont,
                    TextShadowColor = def.Navigation.titleShadowColor,
                    TextShadowOffset = def.Navigation.titleShadowOffset,
                    TextColor = def.Navigation.titleColor
                };


                UINavigationBar.Appearance.SetTitleTextAttributes(attr);

                if (def.Navigation.adjustmentAmount != 0)
                    UINavigationBar.Appearance.SetTitleVerticalPositionAdjustment(def.Navigation.adjustmentAmount, UIBarMetrics.Default);



                var backattr = new UITextAttributes
                {
                    Font = def.Buttons.titleFont,
                    TextColor = def.Buttons.titleColor
                };

                UIBarButtonItem.Appearance.SetTitleTextAttributes(backattr, UIControlState.Normal);

            }

			if(withCustomizer!=null)
				withCustomizer(def);
        }

        public void ApplyBackground(UIView v)
        {
            try
            {
				if(CurrentBackground == BackgroundType.Default){
				v.BackgroundColor = UIColor.FromPatternImage(CurrentAppearance.View.backGroundImage);
				}
				else{
					v.BackgroundColor = UIColor.FromPatternImage(UIImage.FromBundle("TemplateImages/Backgrounds/"+CurrentBackground.ToString().ToLower()));

				}
                //v.AddSubview(new UIImageView(CurrentAppearance.View.backGroundImage));
            }
            catch { }
        }

        public void ApplyNavbar(UINavigationBar inst)
        {
            try
            {
                if (CurrentAppearance.Navigation.backgroundTranslucent)
                    inst.Translucent = true;
            }
            catch { }
        }


    }

    public static class AppearanceExtensions
    {
        public static void ApplyBackground(this UIViewController instance)
        {
            AppearanceManager.Instance.ApplyBackground(instance.View);
        }

        public static void ApplyNavbar(this UIViewController instance)
        {
			if(AppearanceManager.Instance.CurrentTheme == ThemeChoice.None)
				return;

			if (instance is UINavigationController)
            {
                ((UINavigationController)instance).NavigationBar.Translucent = AppearanceManager.Instance.CurrentAppearance.Navigation.backgroundTranslucent;
            }
            else if (instance.NavigationController != null)
                if (instance.NavigationController.NavigationBar != null)
                    AppearanceManager.Instance.ApplyNavbar(instance.NavigationController.NavigationBar);
        }

		public static void ApplyButtonStyle(this UIButton btn, ButtonStyleTypes withStyle, bool withDropShadow=false)
		{
			if(withStyle == ButtonStyleTypes.None)
				return;

			//apply the background drawable and cap insets
			try{
			UIImage bgReg = null;
			UIImage bgHigh = null;

			//build the string out of the enum passed in...
			var imgR = string.Format("TemplateImages/Buttons/{0}Button",withStyle.ToString().ToLower());
			var imgH = string.Format("TemplateImages/Buttons/{0}ButtonHighlight",withStyle.ToString().ToLower());

			bgReg = UIImage.FromBundle(imgR).CreateResizableImage(new UIEdgeInsets(18,18,18,18));
			bgHigh = UIImage.FromBundle(imgH).CreateResizableImage(new UIEdgeInsets(18,18,18,18));

			btn.SetBackgroundImage(bgReg,UIControlState.Normal);
			btn.SetBackgroundImage(bgHigh,UIControlState.Highlighted);
			
			}
			catch{}

			if(withStyle == ButtonStyleTypes.White || withStyle == ButtonStyleTypes.Grey || withStyle == ButtonStyleTypes.Tan)
				if(btn.TitleColor(UIControlState.Normal) == UIColor.White)
					btn.SetTitleColor(UIColor.Black,UIControlState.Normal);

			if(withDropShadow){
				btn.Layer.ShadowRadius = 3.0f;
				btn.Layer.ShadowColor = UIColor.Black.CGColor;
				btn.Layer.ShadowOffset = new SizeF(0.0f,1.0f);
				btn.Layer.ShadowOpacity = .5f;
				btn.Layer.MasksToBounds = false;
			}
		}

		public static RectangleF AddHeight(this RectangleF rec, float amount)
		{
			return new RectangleF(rec.X,rec.Y,rec.Width,rec.Height += amount);
		}

		
		public static RectangleF AddWidth(this RectangleF rec, float amount)
		{
			return new RectangleF(rec.X,rec.Y,rec.Width += amount,rec.Height);
		}

		public static RectangleF GetMaxUseableFrame(this UIViewController view)
		{
			float kNavigationBarPortraitHeight = 44;
			float kNavigationBarLandscapeHeight = 34;
			float kToolBarHeight = 49;
			var self = view;
			float height = 0;
			
			// Start with the screen size minus the status bar if present
			RectangleF maxFrame = UIScreen.MainScreen.Bounds;
			RectangleF appFrame = UIScreen.MainScreen.ApplicationFrame;

			var diff = maxFrame.Height - appFrame.Height;

			height = maxFrame.Size.Height - diff;
			
			// If the orientation is landscape left or landscape right then swap the width and height
			if (self.InterfaceOrientation == UIInterfaceOrientation.LandscapeLeft || self.InterfaceOrientation == UIInterfaceOrientation.LandscapeRight) 
			{
				float temp = maxFrame.Size.Height;
				height = maxFrame.Size.Width;
				maxFrame.Size.Width = temp;
			}
			
			// Take into account if there is a navigation bar present and visible (note that if the NavigationBar may
			// not be visible at this stage in the view controller's lifecycle.  If the NavigationBar is shown/hidden
			// in the loadView then this provides an accurate result.  If the NavigationBar is shown/hidden using the
			// navigationController:willShowViewController: delegate method then this will not be accurate until the
			// viewDidAppear method is called.
			if (self.NavigationController!=null) {
				if (self.NavigationController.NavigationBarHidden == false) {
					
					// Depending upon the orientation reduce the height accordingly
					if (self.InterfaceOrientation == UIInterfaceOrientation.LandscapeLeft || self.InterfaceOrientation == UIInterfaceOrientation.LandscapeRight) 
					{
						height -= kNavigationBarLandscapeHeight;
						maxFrame.Size.Height -= kNavigationBarLandscapeHeight;
					}
					else {
						height -= kNavigationBarPortraitHeight;
						maxFrame.Size.Height -= kNavigationBarPortraitHeight;
					}
				}
			}
			
			// Take into account if there is a toolbar present and visible
			if (self.TabBarController!=null) {
				if (!self.TabBarController.View.Hidden) {
					height -= kToolBarHeight;
					maxFrame.Size.Height -= kToolBarHeight;
				}
			}

			return new RectangleF(maxFrame.X, maxFrame.Y, maxFrame.Width, height);
			//return maxFrame;
		}
    }

	#region some helper views

	public class AutoSizeScrollView : UIScrollView
	{
		public float ExtraHeight {get;set;}
		public float ExtraWidth {get;set;}

		public override void LayoutSubviews ()
		{
			var self = this;

			float scrollViewHeight = 0.0f;
			self.ShowsHorizontalScrollIndicator = false;
			self.ShowsVerticalScrollIndicator = false;
			foreach (UIView view in self.Subviews)
			{
				if (!view.Hidden)
				{
					var y = view.Frame.Location.Y;
					var h = view.Frame.Size.Height;
					if (y + h > scrollViewHeight)
					{
						scrollViewHeight = h + y;
					}
				}
			}
			self.ShowsHorizontalScrollIndicator=true;
			self.ShowsVerticalScrollIndicator = true;
			self.ContentSize = new SizeF(self.Frame.Size.Width += ExtraWidth, scrollViewHeight+= ExtraHeight);
		}
	}



	#endregion


    #region code to help with definitions of appearance resources
    public class AppearanceDef
    {
        public ButtonDef Buttons { get; set; }
        public NavigationBarDef Navigation { get; set; }
        public ViewDef View { get; set; }

    }

    public class ViewDef
    {
        public UIImage backGroundImage { get; set; }
    }

    public class ButtonDef
    {
        //back button stuff
        public UIImage backButtonImage { get; set; }
        public float[] capInsets { get; set; }

        //text options for back button
        public UIColor titleColor { get; set; }
        public UIFont titleFont { get; set; }

    }

    public class NavigationBarDef
    {
        public float adjustmentAmount { get; set; }

        public UIImage backGroundImage { get; set; }
        public UIFont titleFont { get; set; }
        public UIColor titleColor { get; set; }
        public UIColor titleShadowColor { get; set; }
        public UIOffset titleShadowOffset { get; set; }

        public bool backgroundTranslucent { get; set; }
    }



    #endregion

    #region theme definitions
	public enum ButtonStyleTypes
	{
		None,
		Black,
		Blue,
		Green,
		Grey,
		Orange,
		Tan,
		White
	}

	public enum BackgroundType
	{
		Default,
		Binding_Dark,
		Diamond_Upholstery,
		GPlayPattern,
		Gradient_Squares,
		Grey,
		Grey_Wash_Wall,
		Hexabump,
		Noisy_Grid,
		Retina_Wood,
		Square_BG,
		Bright_Squares
	}


    public enum ThemeChoice
    {
		None,
        Piccadilly1,
        Piccadilly2,
        Piccadilly3,
        VauxhallBlue,
        VauxhallGreen,
        VauxhallOrange,
        VauxhallRed,
        PimlicoBirch,
        PimlicoCherry,
        PimlicoOak,
        HendonDark,
        HendonGrey,
        HendonLight,
        CamdenBlue,
        CamdenGreen,
        CamdenMagenta,
        CamdenOrange,
        CamdenRed,
        HolbornDark,
        HolbornGreen,
        KensingtonForrest,
        KensingtonNavy,
        KensingtonPurple,
        BelsizeBlue,
        BelsizeGreen,
        BelsizeRed,
        Richmond1,
        Richmond2,
        Richmond3,
        Richmond4,
        Richmond5,
        Richmond6,
        Richmond7,
        Richmond8,
        SloaneDark,
        SloaneLight,
        SloaneMedium
    }
    #endregion

}

