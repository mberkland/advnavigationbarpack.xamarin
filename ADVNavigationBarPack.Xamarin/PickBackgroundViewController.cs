
using System;
using System.Drawing;

using MonoTouch.Foundation;
using MonoTouch.UIKit;

namespace ADVNavigationBarPack.Xamarin
{
	public partial class PickBackgroundViewController : UIViewController
	{
		//apply the style to stuff now
		ThemeChoice theme;
		BackgroundType bg;

		public PickBackgroundViewController (ThemeChoice t) : base ("PickBackgroundViewController", null)
		{
			theme = t;
		}
		
		public override void DidReceiveMemoryWarning ()
		{
			// Releases the view if it doesn't have a superview.
			base.DidReceiveMemoryWarning ();
			
			// Release any cached data, images, etc that aren't in use.
		}
		
		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();

			// Perform any additional setup after loading the view, typically from a nib.
			var adapter = new EnumTableAdapter<BackgroundType>((choice)=>{
				
				var first = new ShowStyleViewController(this.theme,choice){ Title = "TITLE"};
				this.PresentViewController(first,true,null);
				
			}, 
			withImageCustomizer: (choice)=>{
				try{
					return UIImage.FromBundle("TemplateImages/Backgrounds/"+choice.ToString().ToLower());
				}
				catch{}
				return null;
			} );
			
			this.tblBGStyles.Source = adapter;

			
			// Perform any additional setup after loading the view, typically from a nib.
		}
	}
}

